package bunco;

import framework.CustomList;
import framework.Jeu;
import framework.Joueur;
import framework.ScoreStrategy;

/**
 * Cette classe, qui implémente ScoreStrategy dans un contexte de patron stratégie, sers à définir les différentes règles pour les calcul du scores de jeu.
 *
 * @version 1.0
 */
public class BuncoScore implements ScoreStrategy {


    /**
     * Méthode pour calculer le score d'un joueur lors du tour.
     *
     * @param jeu
     * @return score (int)
     */
    @Override
    public int calculerScoreTour(Jeu jeu) {

        int[] resultatsDes = jeu.getResultatDes();
        int rondeActuelle = jeu.getRondeActuelle();
        int score = 0;
        int nombresDeDesEgalALaRonde = 0;

        //On vérifie dans les dés si il y en a qui son égaux au numéro de la ronde (tour).
        for (int resultatsDe : resultatsDes) {
            if (resultatsDe == rondeActuelle)
                nombresDeDesEgalALaRonde++;
        }

        //On vérifie si les 3 dés sont égaux.
        if (resultatsDes[0] == resultatsDes[1] && resultatsDes[0] == resultatsDes[2]) {
            score = 5;
        }

        //Dépendament du nombre de dés égaux au numéro de ronde, on donne un certain score au joueur.
        switch (nombresDeDesEgalALaRonde) {
            case 1:
                score = 1;
                break;
            case 2:
                score = 2;
                break;
            case 3:
                score = 21;
                break;
        }
        return score;
    }

    /**
     * Méthode qui calcul le vainqueur du jeu.
     *
     * @param jeu
     * @return vainqueur (Joueur)
     */
    @Override
    public Joueur calculerVainqueur(Jeu jeu) {

        CustomList listeDeJoueurs = jeu.getJoueurs().getListeJoueurs();
        Joueur vainqueur = (Joueur) listeDeJoueurs.get(0);

        for (int i = 0; i < listeDeJoueurs.size(); i++) {
            Joueur joueur = (Joueur) listeDeJoueurs.get(i);
            if (vainqueur.compareTo(joueur) < 0)
                vainqueur = joueur;
        }
        return vainqueur;
    }


    /**
     * Méthode qui utilise un Bubble Sort pour trier les joueurs de la liste en ordre du plus grand score au plus petit.
     *
     * @param jeu
     * @return liste (Liste des joueurs en ordre de score)
     */
    public Object[] classementFinal(Jeu jeu) {
        CustomList listeDeJoueurs = jeu.getJoueurs().getListeJoueurs();
        Object[] liste = listeDeJoueurs.toArray();
        int n = liste.length;
        Joueur temp = null;

        //Bubble Sort
        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                if (((Joueur) liste[j - 1]).compareTo(liste[j]) < 0) {
                    temp = (Joueur) liste[j - 1];
                    liste[j - 1] = liste[j];
                    liste[j] = temp;
                }
            }
        }
        return liste;
    }

}
