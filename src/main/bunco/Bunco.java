package bunco;

import framework.*;


/**
 * Cette classe, qui extends Jeu dans un contexte de patron Template Method, sers à créer une instance du jeu Bunco avec les différentes initialisations.
 *
 * @version 1.0
 */
public class Bunco extends Jeu {


    /**
     * Constructeur qui démarre l'instance du jeu avec le nom des joueurs qui sont sauvegardé dans la classe mère.
     *
     * @param nomsDeJoueurs
     */
    public Bunco(String[] nomsDeJoueurs) {
        super(nomsDeJoueurs);
    }


    /**
     * Méthode qui initialise les règles du jeu en faisant appel à une stratégie pour calculer le pointage.
     */
    @Override
    protected void initializerRegles() {
        nbRondes = 6;
        scoreStrategy = new BuncoScore();
    }


    /**
     * Méthode qui initialise une collection des joueurs à l'aide d'un tableau avec le nom des joueurs passé en paramètre.
     *
     * @param nomsDeJoueurs
     */
    @Override
    protected void initializerJoueurs(String[] nomsDeJoueurs) {
        joueurs = Fabrique.creerCollectionJoueur();
        for (String nom : nomsDeJoueurs) {
            joueurs.getListeJoueurs().add(Fabrique.creerJoueur(nom));
        }
        joueurActuel = (Joueur) joueurs.getListeJoueurs().get(0);
    }

    /**
     * Méthode qui initialise la collection des dés.
     */
    @Override
    protected void initializerDes() {
        des = Fabrique.creerCollectionDe();
        des.getListeDes().add(Fabrique.creerDe(6));
        des.getListeDes().add(Fabrique.creerDe(6));
        des.getListeDes().add(Fabrique.creerDe(6));
    }


    /**
     * Méthode qui calcule le score qu'un joueur à obtenu lors de son lancé de dés et qui retourne une instance de InformationTour
     * avec toutes les informations nécessaires comme le joueur actuel, le prochain joueur, les dés lancé, etc.
     *
     * @return InformationTour
     */
    @Override
    public InformationTour calculerScoreTour() {
        int score = 0;

        Iterateur iterateurDes = des.creerIterateur();
        while (iterateurDes.hasNext()) {
            ((De) iterateurDes.next()).lancer();
        }
        score = scoreStrategy.calculerScoreTour(this);

        return gererTour(score);
    }

    /**
     * Méthode qui gère le tour des joueurs dépendamment de leur score.
     * Passe le tour si le score est de 0.
     *
     * @return InformationTour
     */
    public InformationTour gererTour(int score) {

        String joueurActuel = getJoueurActuel().getNom();
        String prochainJoueur;

        boolean finDuJeu = false;

        //On régarde si on doit passer le tour au prochain joueur
        if (score != 0 && score != 21) {
            prochainJoueur = joueurActuel;
            getJoueurActuel().addPointage(score);
        } else {
            if (score != 0) {
                getJoueurActuel().addPointage(score);
            }
            passerLeTour();
            prochainJoueur = getJoueurActuel().getNom();
        }

        //On vérifie si la partie est terminée.
        if (rondeActuelle > nbRondes) {
            finDuJeu = true;
        }

        return new InformationTour(joueurActuel, prochainJoueur, score, finDuJeu);
    }


    /**
     * Méthode qui calcul le vainqueur et qui retourne le joueur gagnant.
     *
     * @return Joueur
     */
    @Override
    protected Joueur calculerVainqueur() {
        return scoreStrategy.calculerVainqueur(this);
    }

}
