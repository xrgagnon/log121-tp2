package framework;

/**
 * Interface du patron itérateur.
 */
public interface Iterateur {

    public boolean hasNext();

    public Object next();
}
