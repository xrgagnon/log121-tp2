package framework;

/**
 * Classe abstraite (Patron Template Method) qui fournis les règles nécessaires à la création d'un jeu.
 *
 * @version 1.0
 */
public abstract class Jeu {

    protected int nbRondes;
    protected CollectionJoueur joueurs;
    protected CollectionDes des;
    protected ScoreStrategy scoreStrategy;
    protected int rondeActuelle = 1;
    protected Joueur joueurActuel;

    /**
     * Constructeur pour définir dans quel ordre les méthodes doivent être exécutés lorsqu'une nouvelle instance du jeu est créée.
     *
     * @param nomsDeJoueurs
     */
    public Jeu(String[] nomsDeJoueurs) {
        initializerRegles();
        initializerJoueurs(nomsDeJoueurs);
        initializerDes();
    }

    /**
     * Méthode abstraite qui initialise les joueurs avec leur nom.
     *
     * @param nomsDeJoueurs
     */
    abstract protected void initializerJoueurs(String[] nomsDeJoueurs);

    /**
     * Méthode abstraite qui initialise les Des.
     */
    abstract protected void initializerDes();

    /**
     * Méthode abstraite qui initialise les règles du jeu.
     */
    abstract protected void initializerRegles();

    /**
     * Méthode abstraite pour gérer les tours des joueurs et la fin de la partie.
     *
     * @param score
     * @return InformationTour
     */
    public abstract InformationTour gererTour(int score);

    /**
     * Méthode abstraite qui joue un tour et calcule le score du joueur.
     *
     * @return InformationTour
     */
    public abstract InformationTour calculerScoreTour();

    /**
     * Méthode abstraite qui calcule le vainqueur et qui retourne le jouer gagnant.
     *
     * @return Joueur
     */
    abstract protected Joueur calculerVainqueur();

    /**
     * Méthode que retourne un array avec le résultat des dés.
     *
     * @return int[]
     */
    public int[] getResultatDes() {
        if (des != null) {
            int[] resultArray = new int[des.getListeDes().size()];

            for (int i = 0; i < des.getListeDes().size(); i++) {
                resultArray[i] = ((De) des.getListeDes().get(i)).getFace();
            }

            return resultArray;
        } else {
            return new int[0];
        }
    }

    /**
     * Méthode qui passe le tour vers le prochain joueur et qui augmente la ronde si le dernier joueur a joué.
     */
    protected void passerLeTour() {
        Boolean joueurTrouvee = false;
        Joueur prochainJoueur;
        Iterateur iterJoueurs = joueurs.creerIterateur();

        do {
            Joueur joueur = (Joueur) iterJoueurs.next();
            if (joueurActuel.getNom().equals(joueur.getNom())) {
                joueurTrouvee = true;
                if (iterJoueurs.hasNext()) {
                    prochainJoueur = (Joueur) iterJoueurs.next();
                } else {
                    Iterateur it = joueurs.creerIterateur();
                    prochainJoueur = (Joueur) it.next();
                    rondeActuelle++;
                }
                setJoueurActuel(prochainJoueur);
            }
        } while (!joueurTrouvee);
    }

    /**
     * Getters et Setters des variables de la classe.
     */
    public CollectionJoueur getJoueurs() {
        return joueurs;
    }

    public CollectionDes getDes() {
        return des;
    }

    public ScoreStrategy getScoreStrategy() {
        return scoreStrategy;
    }

    public int getRondeActuelle() {
        return rondeActuelle;
    }

    public Joueur getJoueurActuel() {
        return joueurActuel;
    }

    public void setJoueurActuel(Joueur joueurActuel) {
        this.joueurActuel = joueurActuel;
    }

    public void setRondeActuelle(int ronde) {
        rondeActuelle = ronde;
    }
}
