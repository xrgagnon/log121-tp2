package framework;

/**
 * Classe du patron itérateur pour itérer à traver une collection de joueurs.
 *
 * @version 1.0
 */
public class JoueurIterateur implements Iterateur {

    private CustomList<Joueur> listeJoueurs;
    private int index;

    /**
     * Constructeur qui crée l'itérateur de joueurs.
     *
     * @param listeJoueurs
     */
    public JoueurIterateur(CustomList<Joueur> listeJoueurs) {
        this.listeJoueurs = listeJoueurs;
    }

    /**
     * Méthode qui régarde si la liste a un autre élément ou si on est à la fin.
     *
     * @return boolean
     */
    @Override
    public boolean hasNext() {
        if (index < listeJoueurs.size()) {
            return true;
        }
        return false;
    }

    /**
     * Méthode qui retourne le prochain élément dans la liste.
     *
     * @return Object
     */
    @Override
    public Object next() {
        if (this.hasNext()) {
            return listeJoueurs.get(index++);
        }
        return null;
    }
}
