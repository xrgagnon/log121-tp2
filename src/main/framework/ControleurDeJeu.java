/**
 * Cadriciel pour un jeu de dés
 *
 * Ce programme simule le jeu de dés Bunco+. Ce programme contiens plusieurs test unitaires et peut-être utilisé comme un cadriciel pour l'implémentation
 * d'autres jeux de dés.
 *
 * @team : 01
 * @author : Xavier-Rowan Gagnon
 * @author : Alejandro Olivares Hernandez
 * @author : Baptiste Tremplay
 * @author : Ngoc Anh Tu Tran
 *
 * @date : 11-11-2020
 * @version : 1.0
 */

package framework;

import java.util.Scanner;

/**
 * Cette classe représente le point de contrôle du jeu. C'est dans cette classe qu'on démarre le jeu et qu’on affiche les résultats dans la console.
 *
 * @version 1.0
 */
public class ControleurDeJeu {

    private static Jeu jeu;
    private static InformationTour tour;
    private static final Scanner input = new Scanner(System.in);


    /**
     * Cette méthode sert de point de départ. C'est ici qu'on démarre le jeu avec les joueurs désirés et qu'on affiche les résultats dans la console.
     *
     * @param args
     */
    public static void main(String[] args) {

        //On affiche un message de bienvenu et on demande le nombre de joueurs
        System.out.println("");
        System.out.println("------------BUNCO+-------------\n");
        System.out.println("Bienvenu au jeu BUNCO+, combien de joueurs? :");
        int nbJoueur = input.nextInt();

        //On crée un tableau pour sauvegarder le nom des jouers désirés et on demande leur nom.
        String[] joueurs = new String[nbJoueur];
        for (int i = 0; i < nbJoueur; i++) {
            System.out.println("Entrer le nom du joueur #" + (i + 1) + ": ");
            joueurs[i] = input.next();
        }

        //On crée un instance du jeu avec les joueurs
        System.out.println("\n\n\n\n\n\n");
        jeu = Fabrique.creerBunco(joueurs);

        //On démarre le jeu
        System.out.println("Le jeu commence!\n");
        do {
            tour = jeu.calculerScoreTour();
            System.out.println("C'est le tour à " + tour.getJoueurActuel());

            for (int x : jeu.getResultatDes()) {
                System.out.print("|" + x + "|  ");
            }
            System.out.println("\n" + tour.getJoueurActuel() + " a obtenu " + tour.getScore() + " points!\n");
        } while (!tour.isFinDuJeu());

        //Quand la partie est terminée on affiche le vainqueur
        System.out.println("la partie est terminé!");
        System.out.println("le grand gagnant est " + jeu.getScoreStrategy().calculerVainqueur(jeu).getNom() + "!\n");
        System.out.println("classement final:");
        Object[] joueursClassement = jeu.getScoreStrategy().classementFinal(jeu);

        //On fait le classement des joueurs
        for (int i = 0; i < joueursClassement.length; i++) {
            String nomJoueur = ((Joueur) joueursClassement[i]).getNom();
            int pointageJoueur = ((Joueur) joueursClassement[i]).getPointage();
            System.out.println(nomJoueur + ": " + pointageJoueur + " points");
        }
    }
}
