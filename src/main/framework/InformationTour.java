package framework;

/**
 * Classe qui fournis certaines informations (Joueur actuel, prochain joueur, score et la fin du jeu) d'un tour.
 *
 * @version 1.0
 */
public class InformationTour {

    private String joueurActuel;
    private String prochainJoueur;
    private int score;
    private boolean finDuJeu;

    /**
     * Contructeur qui crée une instance d'InformationTour avec les différentes informations.
     *
     * @param joueurActuel
     * @param prochainJoueur
     * @param score
     * @param finDuJeu
     */
    public InformationTour(String joueurActuel, String prochainJoueur, int score, boolean finDuJeu) {
        this.joueurActuel = joueurActuel;
        this.prochainJoueur = prochainJoueur;
        this.score = score;
        this.finDuJeu = finDuJeu;
    }

    /**
     * Getter des différentes informations.
     */
    public String getJoueurActuel() {
        return joueurActuel;
    }

    public String getProchainJoueur() {
        return prochainJoueur;
    }

    public int getScore() {
        return score;
    }

    public boolean isFinDuJeu() {
        return finDuJeu;
    }
}
