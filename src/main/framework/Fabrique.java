package framework;

import bunco.Bunco;

/**
 * Classe utilisé pour fabriquer les différentes instances de Dé, Joueurs, Collections et le Jeu Bunco.
 *
 * @version 1.0
 */
public class Fabrique {

    /**
     * Méthode qui crée une instance de De avec son nombre de faces.
     *
     * @param nbDeFaces
     * @return new De
     */
    public static De creerDe(int nbDeFaces) {
        return new De(nbDeFaces);
    }

    /**
     * Méthode qui crée une instance de Joueur avec son nom.
     *
     * @param nom
     * @return new Joueur
     */
    public static Joueur creerJoueur(String nom) {
        return new Joueur(nom);
    }

    /**
     * Méthode qui crée une collection de dés.
     *
     * @return new CollectionDes
     */
    public static CollectionDes creerCollectionDe() {
        return new CollectionDes();
    }

    /**
     * Méthode qui crée une collection de joueurs.
     *
     * @return new CollectionJoueur
     */
    public static CollectionJoueur creerCollectionJoueur() {
        return new CollectionJoueur();
    }

    /**
     * Méthode qui crée un jeu Bunco avec les joueurs.
     *
     * @param nomsDeJoueurs
     * @return new Bunco
     */
    public static Jeu creerBunco(String[] nomsDeJoueurs) {
        return new Bunco(nomsDeJoueurs);
    }

}
