package framework;

/**
 * Classe qui crée une collection de joueurs.
 *
 * @version 1.0
 */
public class CollectionJoueur {
    private CustomList<Joueur> listeJoueurs;

    /**
     * Constructeur qui initialise la collection de joueurs avec une CustomList.
     */
    public CollectionJoueur() {
        this.listeJoueurs = new CustomList<Joueur>();
    }

    /**
     * Méthode getter qui donne la liste des joueurs.
     *
     * @return listeJoueurs (CustomList)
     */
    public CustomList<Joueur> getListeJoueurs() {
        return listeJoueurs;
    }

    /**
     * Méthode qui crée un itérateur pour la liste de joueurs.
     *
     * @return JoueurIterateur
     */
    public Iterateur creerIterateur() {
        return new JoueurIterateur(listeJoueurs);
    }
}

