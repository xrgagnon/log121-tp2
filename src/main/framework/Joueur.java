package framework;

/**
 * Classe qui répresente une instance d'un joueur dans le jeu.
 *
 * @version 1.0
 */
public class Joueur implements Comparable {
    private String nom;
    private int pointage = 0;

    /**
     * Constructeur qui crée une instance de joueur avec le nom désiré.
     *
     * @param nom
     */
    public Joueur(String nom) {
        this.nom = nom;
    }

    /**
     * Getter du pointage
     *
     * @return pointage (int)
     */
    public int getPointage() {
        return pointage;
    }

    /**
     * Méthode pour comparer ce joueur à un autre joueur par rapport à leur pointage. Cette méthode retourne 0 si ils sont égaux,
     * plus grand que 0 si ce joueur a un plus gros pointage ou plus petit que 0 si celui-ci a un plus petit pointage.
     *
     * @param o
     * @return int
     */
    public int compareTo(Object o) {
        Joueur autreJoueur = (Joueur) o;
        return this.pointage - autreJoueur.getPointage();
    }

    /**
     * Getter pour le nom
     *
     * @return nom (String)
     */
    public String getNom() {
        return nom;
    }

    /**
     * Setter pour le pointage
     *
     * @param pointage
     */
    public void setPointage(int pointage) {
        this.pointage = pointage;
    }

    /**
     * Méthode qui ajoute un nombre de points aux points que le joueur avait déjà.
     *
     * @param pointsAAjouter
     */
    public void addPointage(int pointsAAjouter) {
        this.pointage += pointsAAjouter;
    }
}
