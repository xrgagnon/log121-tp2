package framework;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

/**
 * Cette classe est une CostumList qui implémente l'interface Collection de Java.
 *
 * @version 1.0
 */
public class CustomList<E> implements Collection<E> {

    //Grosseur le la liste
    private int size = 0;

    //Capacité par défaut de la liste
    private static final int DEFAULT_CAPACITY = 10;

    private Object elements[];

    public CustomList() {
        elements = new Object[DEFAULT_CAPACITY];
    }

    /**
     * Méthode getter pour la grosseur de la liste.
     *
     * @return size
     */
    public int size() {
        return size;
    }

    /**
     * Méthode qui retourne True si la liste est vide.
     *
     * @return boolean
     */
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * Méthode qui chercher dans la liste pour voir si elle contient l'objet en question.
     *
     * @param o
     * @return boolean
     */
    public boolean contains(Object o) {
        for (int i = 0; i < size; i++) {
            if (elements[i] == o) {
                return true;
            }
        }
        return false;
    }

    /**
     * Méthode qui retourne l'élément à l'index i.
     *
     * @param i
     * @return Element
     */
    public E get(int i) {
        if (i >= size || i < 0) {
            throw new IndexOutOfBoundsException("Index: " + i + ", Size " + i);
        }
        return (E) elements[i];
    }

    /**
     * Méthode qui transforme la liste en Array.
     *
     * @return Object[]
     */
    public Object[] toArray() {
        return Arrays.copyOf(elements, size);
    }

    public <T> T[] toArray(T[] a) {
        return (T[]) Arrays.copyOf(elements, size);
    }

    /**
     * Méthode qui ajoute un élément e à la liste.
     *
     * @param e
     * @return boolean
     */
    public boolean add(E e) {
        if (size == elements.length) {
            ensureCapacity();
        }
        elements[size++] = e;
        return true;
    }

    /**
     * Méthode qui cherche un objet o dans la liste et le supprime. Cette méthode retourne True si elle a bien supprimé l'objet ou
     * false si l'objet n'est pas trouvé.
     *
     * @param o
     * @return boolean
     */
    public boolean remove(Object o) {
        for (int i = 0; i < size; i++) {
            if (elements[i] == o) {
                int numElts = elements.length - (i + 1);
                System.arraycopy(elements, i + 1, elements, i, numElts);
                size--;
                return true;
            }
        }
        return false;
    }

    /**
     * Méthode qui supprime l'élément à l'index i dans la liste.
     *
     * @param i
     * @return item (Element)
     */
    public E remove(int i) {
        if (i >= size || i < 0) {
            throw new IndexOutOfBoundsException("Index: " + i + ", Size " + i);
        }
        Object item = elements[i];
        int numElts = elements.length - (i + 1);
        System.arraycopy(elements, i + 1, elements, i, numElts);
        size--;
        return (E) item;
    }

    /**
     * Méthode qui supprime tous les éléments de la liste.
     */
    public void clear() {
        for (int i = size - 1; i >= 0; i--) {
            remove(i);
        }
    }

    /**
     * Méthode qui s'assure que la liste a une assez grande capacité pour notre besoin en doublent la grosseur de la liste.
     */
    private void ensureCapacity() {
        int newSize = elements.length * 2;
        elements = Arrays.copyOf(elements, newSize);
    }

    /**
     * Méthode qui cherche si la liste contient une collection spécifique d'objets.
     *
     * @param c
     * @return boolean
     */
    public boolean containsAll(Collection<?> c) {
        for (int i = 0; i < c.size(); i++) {
            if (!c.contains(elements[i])) {
                return false;
            }
        }
        return true;
    }

    // pas utilisé pour ce projet
    public Iterator<E> iterator() {
        return null;
    }

    // pas utilisé pour ce projet
    public boolean addAll(Collection<? extends E> c) {
        return false;
    }

    // pas utilisé pour ce projet
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    // pas utilisé pour ce projet
    public boolean retainAll(Collection<?> c) {
        return false;
    }

}

