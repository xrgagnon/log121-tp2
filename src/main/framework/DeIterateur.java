package framework;


/**
 * Classe du patron itérateur pour itérer à traver une collection de dés.
 *
 * @version 1.0
 */
public class DeIterateur implements Iterateur {

    private CustomList<De> listeDes;
    private int index;

    /**
     * Constructeur qui crée l'itérateur des dé.
     *
     * @param listeDes
     */
    public DeIterateur(CustomList<De> listeDes) {
        this.listeDes = listeDes;
    }

    /**
     * Méthode qui régarde si la liste a un autre élément ou si on est à la fin.
     *
     * @return boolean
     */
    @Override
    public boolean hasNext() {
        if (index < listeDes.size()) {
            return true;
        }
        return false;
    }

    /**
     * Méthode qui retourne le prochain élément dans la liste.
     *
     * @return Object
     */
    @Override
    public Object next() {
        if (this.hasNext()) {
            return listeDes.get(index++);
        }
        return null;
    }
}
