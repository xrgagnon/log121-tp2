package framework;

/**
 * Classe qui crée une collection de dés.
 *
 * @version 1.0
 */
public class CollectionDes {
    private CustomList<De> listeDes;

    /**
     * Constructeur qui initialise la collection de dés avec une CustomList.
     */
    public CollectionDes() {
        this.listeDes = new CustomList<De>();
    }

    /**
     * Méthode getter qui donne la liste des dés.
     *
     * @return listeDes (CustomList)
     */
    public CustomList<De> getListeDes() {
        return listeDes;
    }

    /**
     * Méthode qui crée un itérateur pour la liste de dés.
     *
     * @return DeIterateur
     */
    public Iterateur creerIterateur() {
        return new DeIterateur(listeDes);
    }

}
