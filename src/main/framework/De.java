package framework;

import java.util.Random;

/**
 * Classe qui répresente une instance d'un dé dans le jeu.
 *
 * @version 1.0
 */
public class De implements Comparable {

    //La face sur laquelle le dé est tombé.
    private int face;

    //Le nombre de faces du dé
    private int nbFaces;

    private Random rand = new Random();

    /**
     * Constructeur qui crée une instance de dé avec le nombre de faces désiré.
     *
     * @param nbFaces
     */
    public De(int nbFaces) {
        this.nbFaces = nbFaces;
    }

    /**
     * Méthode qui compare deux dés ensemble par rapport à leur face. La méthode retourne 0 si ils sont égaux, plus petit que 0 si le dé sur lequel on est plus petit ou
     * plus grand que 0 s'il est plus grand.
     * (N.B. cette méthode peut fonctionne convenablement seulement quand les deux dés ont été lancé.)
     *
     * @param o
     * @return int
     */
    public int compareTo(Object o) {
        De autreDe = (De) o;
        return this.face - autreDe.getFace();
    }

    /**
     * Getter de la face du dé.
     *
     * @return face (int)
     */
    public int getFace() {
        return face;
    }

    /**
     * Setter pour la face des dés (Utilisé dans les test).
     *
     * @param face
     */
    public void setFace(int face) {
        this.face = face;
    }

    /**
     * Méthode qui lance le dé (donc qui donne un nombre aléatoire entre 1 et 6 (inclus)), qui set la face et qui retourne ce nombre.
     *
     * @return face (int)
     */
    public int lancer() {
        face = rand.nextInt(nbFaces) + 1;
        return face;
    }
}
