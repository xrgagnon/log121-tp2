package framework;

/**
 * Interface du patron stratégie.
 */
public interface ScoreStrategy {

    int calculerScoreTour(Jeu jeu);

    Joueur calculerVainqueur(Jeu jeu);

    Object[] classementFinal(Jeu jeu);

}
