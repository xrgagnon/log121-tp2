package UnitTests;

import framework.CustomList;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

/**
 * Test qui test la classe CustomList.
 *
 * @version 1.0
 */
public class CustomListTest {

    CustomList<Integer> actual;
    ArrayList<Integer> expected;

    /**
     * Setup préalable aux tests de la CustomList.
     */
    @Before
    public void setUp() {
        actual = new CustomList<Integer>();
        actual.add(3);
        actual.add(6);
        actual.add(5);
        actual.add(12);
        actual.add(2);

        expected = new ArrayList<Integer>();
        expected.addAll(actual);
    }

    /**
     * Méthode qui efface ce que le setup après les tests.
     */
    @After
    public void tearDown() {
        actual.clear();
        expected.clear();
    }

    /**
     * Test qui test la grosseur de la liste.
     */
    @Test
    public void size() {
        Assert.assertEquals(expected.size(), actual.size());
    }

    /**
     * Test qui test s'il est bien possible d'effacer tout le contenu d'une liste.
     */
    @Test
    public void clear() {
        actual.clear();
        Assert.assertEquals(0, actual.size());
    }

    /**
     * Test qui vérifie si quand on ajoute un élément à la liste, ça l'ajoute réellement.
     */
    @Test
    public void addTest() {
        int originalSize = actual.size();
        actual.add(89);
        Assert.assertEquals(originalSize + 1, actual.size());
    }
}
