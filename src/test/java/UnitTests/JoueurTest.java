package UnitTests;

import framework.Joueur;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test qui test la classe Joueur.
 *
 * @version 1.0
 */
public class JoueurTest {

    //Joueurs pour les tests.
    Joueur joueur1;
    Joueur joueur2;

    /**
     * Création des joueurs préalables aux tests.
     *
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {
        joueur1 = new Joueur("maxime");
        joueur2 = new Joueur("jean");
    }

    /**
     * Test qui compare deux joueurs et qui vérifie si ça retourne bien 0 quand les deux joueurs on le même nombre de points.
     */
    @Test
    public void t1compareTo() {
        joueur1.setPointage(8);
        joueur2.setPointage(8);
        assertEquals(0, joueur1.compareTo(joueur2));
    }

    /**
     * Test qui compare deux joueurs et qui vérifie si ça retourne bien plus grand que 0 quand le joueur 1 a plus de points que le joueur 2.
     */
    @Test
    public void t2compareTo() {
        joueur1.setPointage(14);
        joueur2.setPointage(5);
        assertTrue(joueur1.compareTo(joueur2) > 0);
    }

    /**
     * Test qui compare deux joueurs et qui vérifie si ça retourne bien plus petit que 0 quand le joueur 1 a moins de points que le joueur 2.
     */
    @Test
    public void t3compareTo() {
        joueur1.setPointage(4);
        joueur2.setPointage(82);
        assertTrue(joueur1.compareTo(joueur2) < 0);
    }
}