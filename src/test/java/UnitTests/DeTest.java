package UnitTests;

import framework.De;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test qui test la classe De.
 *
 * @version 1.0
 */
public class DeTest {

    //Dés pour les tests.
    De de1;
    De de2;

    /**
     * Création des dés préalables aux tests.
     *
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {
        de1 = new De(6);
        de2 = new De(6);
    }

    /**
     * Test qui compare deux dés et qui vérifie si ça retourne bien 0 quand les deux dés on la même face.
     */
    @Test
    public void t1CompareTo() {
        de1.setFace(4);
        de2.setFace(4);
        assertEquals(0, de1.compareTo(de2));
    }

    /**
     * Test qui compare deux dés et qui vérifie si ça retourne bien plus grand que 0 quand la face du dé 1 est plus grande que celle du dé 2.
     */
    @Test // quand la face du de1 est de valeur plus grande que la face du de2
    public void t2CompareTo() {
        de1.setFace(6);
        de2.setFace(4);
        assertTrue(de1.compareTo(de2) > 0);
    }

    /**
     * Test qui compare deux dés et qui vérifie si ça retourne bien plus petit que 0 quand la face du dé 1 est plus petite que celle du dé 2.
     */
    @Test
    public void t3CompareTo() {
        de1.setFace(3);
        de2.setFace(4);
        assertTrue(de1.compareTo(de2) < 0);
    }

    /**
     * Test qui vérifie si lorsqu'on lance un dé la face est bien entre 6 et 1.
     */
    @Test
    public void t1Lancer() {
        int resultat = de1.lancer();
        assertTrue(6 >= resultat && 1 <= resultat);
    }
}