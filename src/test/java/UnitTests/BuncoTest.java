package UnitTests;

import framework.*;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Test qui test la classe Bunco.
 *
 * @version 1.0
 */
public class BuncoTest {

    Jeu jeu;

    /**
     * Setup préalable aux tests.
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {
        String[] joueurs = new String[]{"louis", "felix", "antoine", "je"};
        jeu = Fabrique.creerBunco(joueurs);
    }

    /**
     * Test qui vérifie que lorsque les 3 dés sont identiques au numéro de tour, le pointage du joueur augmente bien de 21 points.
     */
    @Test
    public void t1calculerScoreTour() {
        jeu.setRondeActuelle(3);
        Iterateur iterateurDes = jeu.getDes().creerIterateur();
        while (iterateurDes.hasNext()) {
            ((De) iterateurDes.next()).setFace(3);
        }
        int resulatAttendu = jeu.getScoreStrategy().calculerScoreTour(jeu);
        assertEquals(21, resulatAttendu);
    }

    /**
     * Test qui vérifie que lorsque le joueur obient un Bunco, la main est passée au suivant.
     */
    @Test
    public void t2calculerScoreTour() {
        String joueurActuel = jeu.getJoueurActuel().getNom();
        InformationTour tour = jeu.gererTour(21);
        assertTrue(!joueurActuel.equals(tour.getProchainJoueur()));
    }

    /**
     * Test qui vérifie que lorsque les 3 dés sont identiques, mais pas égaux au numéro de tour, le score du joueur augmente bien de 5 points.
     */
    @Test
    public void t3calculerScoreTour() {
        jeu.setRondeActuelle(2);
        Iterateur iterateurDes = jeu.getDes().creerIterateur();
        while (iterateurDes.hasNext()) {
            ((De) iterateurDes.next()).setFace(3);
        }
        int resulatAttendu = jeu.getScoreStrategy().calculerScoreTour(jeu);
        assertEquals(5, resulatAttendu);
    }

    /**
     * Test qui vérifie que quand le joueur obyient 3 dés égaux, mais différents au numéro de tour, il garde la main.
     */
    @Test
    public void t4calculerScoreTour() {
        String joueurActuel = jeu.getJoueurActuel().getNom();
        InformationTour tour = jeu.gererTour(5);
        assertTrue(joueurActuel.equals(tour.getProchainJoueur()));
    }

    /**
     * Test qui vérifie que lorsqu'un seul dé est identique ai numéro de tour, le score du joueur augmente de 1 point.
     */
    @Test
    public void t5calculerScoreTour() {
        jeu.setRondeActuelle(2);

        De de1 = (De) jeu.getDes().getListeDes().get(0);
        de1.setFace(2);

        De de2 = (De) jeu.getDes().getListeDes().get(1);
        de2.setFace(4);

        De de3 = (De) jeu.getDes().getListeDes().get(2);
        de3.setFace(6);

        int resulatAttendu = jeu.getScoreStrategy().calculerScoreTour(jeu);
        assertEquals(1, resulatAttendu);
    }

    /**
     * Test qui vérifie que lorsque le joueur obtient un seul dé identique au numéro du tour, il garde la main.
     */
    @Test
    public void t6calculerScoreTour() {
        String joueurActuel = jeu.getJoueurActuel().getNom();
        InformationTour tour = jeu.gererTour(1);
        assertTrue(joueurActuel.equals(tour.getProchainJoueur()));
    }

    /**
     * Test qui vérifie que lorsque le joueur ne marque pas de points, la main est passée au suivant.
     */
    @Test
    public void t7calculerScoreTour() {
        String joueurActuel = jeu.getJoueurActuel().getNom();
        InformationTour tour = jeu.gererTour(0);
        assertTrue(!joueurActuel.equals(tour.getProchainJoueur()));
    }

    /**
     * Test qui vérifie si c'est bien le joueur qui a le plus de points qui gagne la partie.
     */
    @Test
    public void calculerVainqueur() {

        Joueur joueur1 = (Joueur) jeu.getJoueurs().getListeJoueurs().get(0);
        joueur1.setPointage(34);

        Joueur joueur2 = (Joueur) jeu.getJoueurs().getListeJoueurs().get(1);
        joueur2.setPointage(45);

        Joueur joueur3 = (Joueur) jeu.getJoueurs().getListeJoueurs().get(2);
        joueur3.setPointage(12);

        Joueur joueur4 = (Joueur) jeu.getJoueurs().getListeJoueurs().get(3);
        joueur4.setPointage(34);

        Joueur joueurGagnant = jeu.getScoreStrategy().calculerVainqueur(jeu);
        assertEquals(joueur2, joueurGagnant);
    }
}