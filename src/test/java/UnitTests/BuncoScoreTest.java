package UnitTests;

import bunco.Bunco;
import bunco.BuncoScore;
import framework.CustomList;
import framework.De;
import framework.Jeu;
import framework.ScoreStrategy;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Test qui test la classe BuncoScore.
 *
 * @version 1.0
 */
public class BuncoScoreTest {

    String[] player = {"Player"};
    ScoreStrategy buncoScore = new BuncoScore();
    Jeu bunco = new Bunco(player);
    CustomList<De> desDuJeu = bunco.getDes().getListeDes();

    /**
     * Setup préalable aux tests.
     */
    @Before
    public void setupBuncoScore() {
        desDuJeu = bunco.getDes().getListeDes();
    }

    /**
     * Test qui vérifie si lorsqu'on a pas les bonnes faces, le score est bien de 0.
     */
    @Test
    public void scoreNul() {
        int expected = 0;
        int actual = 0;

        desDuJeu.get(0).setFace(2);
        desDuJeu.get(1).setFace(2);
        desDuJeu.get(2).setFace(3);

        actual = buncoScore.calculerScoreTour(bunco);
        Assert.assertEquals(expected, actual);
    }

    /**
     * Test qui vérifie si lorsqu'on obtient une bonne face, le score augmente de 1.
     */
    @Test
    public void score1() {
        int expected = 1;
        int actual = 0;

        desDuJeu.get(0).setFace(1);
        desDuJeu.get(1).setFace(2);
        desDuJeu.get(2).setFace(2);

        actual = buncoScore.calculerScoreTour(bunco);
        Assert.assertEquals(expected, actual);
    }

    /**
     * Test qui vérifie si lorsqu'on obtient 2 bonnes faces, le score augmente de 2 points.
     */
    @Test
    public void score2() {
        int expected = 2;
        int actual = 0;

        desDuJeu.get(0).setFace(1);
        desDuJeu.get(1).setFace(1);
        desDuJeu.get(2).setFace(2);

        actual = buncoScore.calculerScoreTour(bunco);
        Assert.assertEquals(expected, actual);
    }

    /**
     * Test qui vérifie si lorsqu'on obtient 3 faces égales mais pas égales au tour, on augmente de 5 points.
     */
    @Test
    public void score3PasBunco() {
        int expected = 5;
        int actual = 0;

        desDuJeu.get(0).setFace(2);
        desDuJeu.get(1).setFace(2);
        desDuJeu.get(2).setFace(2);

        actual = buncoScore.calculerScoreTour(bunco);
        Assert.assertEquals(expected, actual);
    }

    /**
     * Test qui vérifie si lorsqu'on obtient 3 faces égales et égales au tour, on obtiens 21 points.
     */
    @Test
    public void score4Bunco() {
        int expected = 21;
        int actual = 0;

        desDuJeu.get(0).setFace(1);
        desDuJeu.get(1).setFace(1);
        desDuJeu.get(2).setFace(1);

        actual = buncoScore.calculerScoreTour(bunco);
        Assert.assertEquals(expected, actual);
    }
}
